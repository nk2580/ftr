import cli from "cli-ux";
import { Observable, Subscription } from "rxjs";
import { filter } from "rxjs/operators";
import * as _groupBy from "lodash.groupby";
import * as _map from "lodash.map";
import * as _orderBy from "lodash.orderby";

interface nextable {
  next(value: any): void;
}
/**
 * create a fibbonacci numbers array
 *
 * @param size size of the array (starting at 0)
 */
export const makeFibbonacci = (size: number): number[] => {
  if (size === 1) {
    return [0, 1];
  } else {
    var s = makeFibbonacci(size - 1);
    s.push(s[s.length - 1] + s[s.length - 2]);
    return s;
  }
};

/**
 * chacks that a number is inside the fibbonacci array
 *
 * @param number
 */
export const isFibbonacci = (
  number: number,
  fibbonacciNumbers: number[]
): boolean => {
  return fibbonacciNumbers.includes(number);
};

/**
 * runs a callback on an observable for number subscription
 *
 * @param cb
 * @param additionalParams
 * @param messages
 */
export const onObserverSubscribe = (
  filterCb: (message: any) => boolean,
  cb: (message: any, ...args: any[]) => void,
  messages: Observable<any>,
  additionalParams: any[] = []
): Subscription => {
  return messages
    .pipe(filter(filterCb))
    .subscribe((data: any) => cb(data, ...additionalParams));
};

/**
 * onObserverSubscribe for number subscriptions
 *
 * @param cb
 * @param additionalParams
 * @param messages
 */
export const onNumberSubscribe = (
  cb: (message: any, ...args: any[]) => void,
  messages: Observable<any>,
  additionalParams: any[]
): Subscription => {
  return onObserverSubscribe(
    (message: any) => message.subscribe === "numbers",
    cb,
    messages,
    additionalParams
  );
};

/**
 * onObserverSubscribe for number messages
 *
 * @param cb callback to run
 * @param additionalParams
 * @param messages
 */
export const onNumberMessage = (
  cb: (message: any, ...args: any[]) => void,
  messages: Observable<any>,
  additionalParams: any[] = []
): Subscription => {
  return onObserverSubscribe(
    (message: any) => message.type == "numbers",
    cb,
    messages,
    additionalParams
  );
};

/**
 * Message Handler for number values sent to the server
 *
 * @param message
 * @param numbersSupplied
 */
export const numberMessageHandler = (
  message: any,
  numbersSupplied: number[]
) => {
  const { value } = message;
  !isNaN(value) ? numbersSupplied.push(value) : null;
};

/**
 * onObserverSubscribe for number messages
 *
 * @param cb callback to run
 * @param additionalParams
 * @param messages
 */
export const onFibbonacciMessage = (
  cb: (message: any, ...args: any[]) => void,
  messages: Observable<any>,
  fibbonacciNumbers: number[]
): Subscription => {
  return onObserverSubscribe(
    (message: any) =>
      message.type == "numbers" &&
      isFibbonacci(message.value, fibbonacciNumbers),
    cb,
    messages
  );
};

/**
 * Message Handler for number values sent to the server
 *
 * @param message
 * @param numbersSupplied
 */
export const fibbonacciMessageHandler = (message: any) => {
  console.log("FIB");
};

/**
 * onObserverSubscribe for number messages
 *
 * @param cb callback to run
 * @param additionalParams
 * @param messages
 */
export const onNumberUnsibscribe = (
  cb: (message: any, ...args: any[]) => void,
  messages: Observable<any>,
  additionalParams: any[] = []
): Subscription => {
  return onObserverSubscribe(
    (message: any) => message.unsubscribe == "numbers",
    cb,
    messages,
    additionalParams
  );
};

/**
 * Reduces numbers array into an object of frequency
 *
 * @param numbers
 */
export const reduceNumbers = (numbers: number[]): any => {
  const response = {};
  _orderBy(_map(_groupBy(numbers, v => v), v => v), v => v.length, [
    "desc"
  ]).map(number => {
    const [key] = number;
    response[key] = number.length;
  });
  return response;
};

/**
 * Prints the numbers in the correct format into the console
 *
 * @param numbers
 */
export const printNumbers = (numbers: number[]): void => {
  console.log(formatReducedNumbers(reduceNumbers(numbers)));
};

/**
 * formats the frequenct for printing
 *
 * @param frequency
 */
export const formatReducedNumbers = (frequency: any): string => {
  return (
    "\n" +
    JSON.stringify(frequency)
      .replace('"', "")
      .replace("{", "")
      .replace("}", "")
  );
};

/**
 * Promt the user for a number input
 *
 * @param subject
 * @param frequency
 * @param numbersSupplied
 */
export const promptForNumber = async (
  subject: nextable,
  frequency: number,
  numbersSupplied: number[]
) => {
  // prompt the user for the interval in seconds
  const input = await cli.prompt(
    `Please enter the ${numbersSupplied.length ? "next" : "first"} number`
  );

  console.log(input);

  switch (input) {
    case "halt":
      subject.next({ unsubscribe: "numbers" });
      break;
    case "resume":
      subject.next({ subscribe: "numbers", seconds: frequency });
      break;
    case "quit":
      subject.next({ unsubscribe: "numbers" });
      printNumbers(numbersSupplied);
      console.log("Thankyou for using this application.");
      process.exit(0);
      break;
    default:
      if (!isNaN(input)) {
        subject.next({ type: "numbers", value: parseInt(input, 10) });
      } else {
        console.log(
          "unrecognised input, please supply an number or one of ['halt', 'resume', 'quit']"
        );
      }
      break;
  }
};

/**
 * Promt the user fot the interval when displaying frequency
 *
 * @param subject
 */
export const promptForSeconds = async (subject: nextable) => {
  const frequency = await cli.prompt(
    "Please input the number of time in seconds between emitting numbers and their frequency"
  );
  if (!isNaN(frequency)) {
    subject.next({
      subscribe: "numbers",
      seconds: parseInt(frequency, 10) * 1000
    });
  } else {
    console.log("Invliad Input: please supply a number!");
    promptForSeconds(subject);
  }
};