import {
  makeFibbonacci,
  isFibbonacci,
  onObserverSubscribe,
  onNumberSubscribe,
  onNumberMessage,
  numberMessageHandler,
  onFibbonacciMessage,
  fibbonacciMessageHandler,
  onNumberUnsibscribe,
  reduceNumbers,
  printNumbers,
  formatReducedNumbers,
  promptForNumber,
  promptForSeconds,
} from "./app";
import { Subject } from "rxjs";
import * as bddStdin from "bdd-stdin";

console.log = jest.fn();

describe("makeFibbonacci", () => {
  it("returns an array of numbers that has a length one larger than the numbers supplied", () => {
    const numbers = makeFibbonacci(2);
    expect(numbers.length).toBe(3);
  });
});

describe("isFibbonacci", () => {
  it("runs an array includes check on the numbers passed to it", () => {
    const numbers = makeFibbonacci(12);
    expect(isFibbonacci(44, numbers)).toBe(false);
    expect(isFibbonacci(1, numbers)).toBe(true);
  });
});

describe("onObserverSubscribe", () => {
  it("runs a callback and a filter with some params on an observable", () => {
    // build our obersvable subject
    const subject = new Subject();
    const received = subject.asObservable();
    const messages = [1, 2, 3];

    const subscriber = onObserverSubscribe(
      (message: any) => message.type == "numbers",
      (message: any, messages: any) => {
        expect(message.type).toEqual("numbers");
        messages.push(4);
      },
      received,
      [messages]
    );
    expect(messages).toEqual([1, 2, 3]);

    subject.next({ type: "numbers" });
    expect(messages).toEqual([1, 2, 3, 4]);

    subject.next({ type: "not handled" });
    expect(messages).toEqual([1, 2, 3, 4]);
  });

  it("passes an empty array of additional params to the callback if none are specified", () => {
    // build our obersvable subject
    const subject = new Subject();
    const received = subject.asObservable();
    const messages = [1, 2, 3];

    const subscriber = onObserverSubscribe(
      (message: any) => message.type == "numbers",
      (message: any, extra: any) => {
        expect(extra).toBe(undefined);
        messages.push(extra);
      },
      received
    );
    expect(messages).toEqual([1, 2, 3]);

    subject.next({ type: "numbers" });
    expect(messages).toEqual([1, 2, 3, undefined]);

    subject.next({ type: "not handled" });
    expect(messages).toEqual([1, 2, 3, undefined]);
  });
});

describe("onNumberSubscribe", () => {
  it("onObserverSubscribe with a filter on message.subscribe: 'numbers'", () => {
    // build our obersvable subject
    const subject = new Subject();
    const received = subject.asObservable();
    const messages = [1, 2, 3];

    const numberSubscription = onNumberSubscribe(
      (message: any, messages: any) => {
        messages.push(4);
      },
      received,
      [messages]
    );

    expect(messages).toEqual([1, 2, 3]);

    subject.next({ subscribe: "numbers" });
    expect(messages).toEqual([1, 2, 3, 4]);

    subject.next({ type: "numbers" });
    expect(messages).toEqual([1, 2, 3, 4]);
  });
});

describe("onNumberMessage", () => {
  it("onObserverSubscribe with a filter on message.type: 'numbers'", () => {
    // build our obersvable subject
    const subject = new Subject();
    const received = subject.asObservable();
    const messages = [1, 2, 3];

    const numberSubscription = onNumberMessage(
      (message: any, messages: any) => {
        messages.push(4);
      },
      received,
      [messages]
    );

    expect(messages).toEqual([1, 2, 3]);

    subject.next({ type: "numbers" });
    expect(messages).toEqual([1, 2, 3, 4]);

    subject.next({ something: "not handled" });
    expect(messages).toEqual([1, 2, 3, 4]);
  });
});

describe("numberMessageHandler", () => {
  it("adds message value to array of numbers", () => {
    const numbers = [];
    const message = { value: 12 };
    const badMessage = { value: "something" };
    numberMessageHandler(message, numbers);
    expect(numbers).toEqual([12]);
    numberMessageHandler(badMessage, numbers);
    expect(numbers).toEqual([12]);
  });
});

describe("onFibbonacciMessage", () => {
  it("onObserverSubscribe with a filter on message.type: 'numbers' and number in fibbonacci", () => {
    // build our obersvable subject
    const subject = new Subject();
    const received = subject.asObservable();
    const messages = [1, 2, 3];

    const numbers = makeFibbonacci(12);

    onFibbonacciMessage(
      (message: any) => {
        messages.push(4);
      },
      received,
      numbers
    );

    expect(messages).toEqual([1, 2, 3]);

    subject.next({ type: "numbers", value: 1 });
    expect(messages).toEqual([1, 2, 3, 4]);

    subject.next({ type: "numbers", value: 1245789 });
    expect(messages).toEqual([1, 2, 3, 4]);
  });
});

describe("fibbonacciMessageHandler", () => {
  it("calls console .log with FIB", () => {
    fibbonacciMessageHandler({});
    expect(console.log).toHaveBeenCalledWith("FIB");
  });
});

describe("onNumberUnsibscribe", () => {
  it("onObserverSubscribe with a filter on message.unsubscribe: 'numbers' ", () => {
    // build our obersvable subject
    const subject = new Subject();
    const received = subject.asObservable();
    const messages = [1, 2, 3];

    const numberSubscription = onNumberUnsibscribe(
      (message: any, messages: any) => {
        messages.push(4);
      },
      received,
      [messages]
    );

    expect(messages).toEqual([1, 2, 3]);

    subject.next({ unsubscribe: "numbers" });
    expect(messages).toEqual([1, 2, 3, 4]);

    subject.next({ type: "numbers" });
    expect(messages).toEqual([1, 2, 3, 4]);
  });
});

describe("reduceNumbers", () => {
  it("should reduce numbers array into an object listing number=>frequnecy as key=>value ordered by frequency", () => {
    const numbers = [1, 1, 1, 5, 5, 5, 5];
    const frequnecy = reduceNumbers(numbers);
    const expected = { 5: 4, 1: 3 };
    expect(frequnecy).toEqual(expected);
  });
});

describe("printNumbers", () => {
  it("should console log the formatted reduced numbers", () => {
    const numbers = [1, 1, 1, 5, 5, 5, 5];
    printNumbers(numbers);
    expect(console.log).toHaveBeenLastCalledWith(
      formatReducedNumbers(reduceNumbers(numbers))
    );
  });
});

describe("formatReducedNumbers", () => {
  it("should return formatted reduced numbers", () => {
    const numbers = [1, 1, 1, 5, 5, 5, 5];
    const print = formatReducedNumbers(reduceNumbers(numbers));
    const expected =
      "\n" +
      JSON.stringify(reduceNumbers(numbers))
        .replace('"', "")
        .replace("{", "")
        .replace("}", "");
    expect(print).toEqual(expected);
  });
});

describe("promptForNumber", () => {
  it("should call next on the subject when a number is supplied", async () => {
    const responseFrequency = 1200;
    const numbersSupplied = [];
    let subject = {
      next: jest.fn()
    };
    bddStdin("12");
    await promptForNumber(subject, responseFrequency, numbersSupplied);
    expect(subject.next).toHaveBeenCalledWith({
      type: "numbers",
      value: parseInt("12", 10)
    });
  });
  it("should call next on the subject with the unsubscribe paylod if halt is suplied", async () => {
    const responseFrequency = 1200;
    const numbersSupplied = [];
    let subject = {
      next: jest.fn()
    };
    bddStdin("halt");
    await promptForNumber(subject, responseFrequency, numbersSupplied);
    expect(subject.next).toHaveBeenCalledWith({ unsubscribe: "numbers" });
  });
  it("should call next on the subject with the subscribe paylod if resume is suplied", async () => {
    const responseFrequency = 1200;
    const numbersSupplied = [];
    let subject = {
      next: jest.fn()
    };
    bddStdin("resume");
    await promptForNumber(subject, responseFrequency, numbersSupplied);
    expect(subject.next).toHaveBeenCalledWith({
      subscribe: "numbers",
      seconds: responseFrequency
    });
  });
  it("should throw an error when unexoected strings are passed", async () => {
    const responseFrequency = 1200;
    const numbersSupplied = [];
    let subject = {
      next: jest.fn()
    };
    bddStdin("something");
    await promptForNumber(subject, responseFrequency, numbersSupplied);
    expect(console.log).toHaveBeenLastCalledWith(
      "unrecognised input, please supply an number or one of ['halt', 'resume', 'quit']"
    );
  });
  it("should call next on the subject with the unsubscribe paylod if quit is suplied and should print then exit the application", async () => {
    const responseFrequency = 1200;
    const numbersSupplied = [3, 3, 1, 4, 5];
    let subject = {
      next: jest.fn()
    };
    bddStdin("quit");
    const mockExit = jest.spyOn(process, "exit").mockImplementation();
    await promptForNumber(subject, responseFrequency, numbersSupplied);
    expect(subject.next).toHaveBeenCalledWith({ unsubscribe: "numbers" });
    const finalnumbers = formatReducedNumbers(reduceNumbers(numbersSupplied));
    expect(console.log).toHaveBeenCalledWith(finalnumbers);
    expect(mockExit).toHaveBeenCalledWith(0);
  });
});

describe("promptForSeconds", () => {
  it("should call the subscription with the number multipled by 1000", async () => {
    let subject = {
      next: jest.fn()
    };
    bddStdin("1");
    await promptForSeconds(subject);
    expect(subject.next).toHaveBeenCalledWith({
      subscribe: "numbers",
      seconds: 1000
    });
  });
  it("should log to the console if no number is passed", async () => {
    let subject = {
      next: jest.fn()
    };
    bddStdin("error");
    await promptForSeconds(subject);
    expect(console.log).toHaveBeenCalledWith(
      "Invliad Input: please supply a number!"
    );
  });
});
