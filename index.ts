import { makeFibbonacci, onNumberSubscribe, printNumbers, onNumberMessage, numberMessageHandler, fibbonacciMessageHandler, onFibbonacciMessage, onNumberUnsibscribe, promptForSeconds, promptForNumber } from "./src/app";
import { Subject } from "rxjs";

// because of the indexes 99 = 100 items
const fibbonacciNumbers = makeFibbonacci(99);

// placeholder for the interval
let responseFrequency;
let responseInterval;

// placeholder for the numbers sent during the life of the server
const numbersSupplied = [];

// build our obersvable subject
const subject = new Subject();
const received = subject.asObservable();

// handle messges sent to subscribe to the number channel
onNumberSubscribe(
  (message: any) => {
    const { seconds } = message;
    responseFrequency = seconds;
    responseInterval = setInterval(
      () => printNumbers(numbersSupplied),
      seconds
    );
  },
  received,
  [responseInterval, numbersSupplied]
);

// handle the number messages
onNumberMessage(numberMessageHandler, received, [numbersSupplied]);

//handle the fibbonacci messages
onFibbonacciMessage(fibbonacciMessageHandler, received, fibbonacciNumbers);

// handle the unsubscribe actions
onNumberUnsibscribe(message => {
  clearInterval(responseInterval);
  responseInterval = null;
}, received);

// interact with the user
try {
  promptForSeconds(subject);
  while (true) {
    promptForNumber(subject, responseFrequency, numbersSupplied);
  }
} catch (err) {
  console.log(err);
}
